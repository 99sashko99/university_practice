﻿<?php
session_start();
if ($_SESSION['flag']!=1) {
$_SESSION['flag']=0;
header("Location: https://bill.univ.kiev.ua/registration/index.php");
exit();
}
function getIp() {
  $keys = [
    'HTTP_CLIENT_IP',
    'HTTP_X_FORWARDED_FOR',
    'REMOTE_ADDR'
  ];
  foreach ($keys as $key) {
    if (!empty($_SERVER[$key])) {
      $ip = trim(end(explode(',', $_SERVER[$key])));
      if (filter_var($ip, FILTER_VALIDATE_IP)) {
        return $ip;
      }
    }
  }
}
$_SESSION["ip"] =getIP();
?>
//
<html>
 <head>
  <meta charset="windows-1251">
  <title>Сторінка реєстрації нового користувача</title>
 </head>
 <body>
  <form name="form_registr" onsubmit="alert('Дякуємо за реєстрацію. Завантажте реєстраційну анкету, роздрукуйте її та підійдіть з нею та документом що посвідчує вашу особу до кімнати 111 або 110б на ІОЦ в ро
бочі дні з 10 до 17 години')" action="pdfpage.php" method="post">
    <h2>Форма реєстрації нового користувача</h2>
 <h4>Заповніть поля форми</h4>
<p>
      <select name="who" id="student"onchange="Check(this.value)" required>
      <option disabled selected>Оберіть хто ви</option>
        <option value="студент" >студент/студентка</option>
        <option value="співробітник" >співробітник/співробітниця</option>
     </select>
    </p>
   <p>
     <select name="kurs" id="kurs"onchange="Next()" disabled>
    <option id="wrong" disabled selected>Виберіть курс</option>
    <optgroup label="Бакалавр">
     <option value="1">1</option>
     <option value="2">2</option>
     <option value="3">3</option>
     <option value="4">4</option>
    </optgroup>
     <optgroup label="Магістр">
     <option value="5">1</option>
     <option value="6">2</option>
 <option value="7">3</option>
 <option value="8">4</option>
 <option value="9">5</option>
 <option value="10">6</option>
    </optgroup>
</select>
  </p>
 <p>
   <select name="faculty" id="faculty" onchange="Next()" disabled>
    <option id="wrong" disabled selected>Оберіть факультет</option>
    <option value="Географічний факультет"> Географічний факультет </option>
        <option value="Економічний факультет"> Економічний факультет </option>
        <option value="Історичний факультет"> Історичний факультет </option>
        <option value="Механіко-математичний факультет"> Механіко-математичний факультет </option>
        <option value="Факультет інформаційних технологій"> Факультет інформаційних технологій </option>
        <option value="Факультет комп'ютерних наук та кібернетики"> Факультет комп'ютерних наук та кібернетики </option>
        <option value="Факультет психології"> Факультет психології </option>
        <option value="Факультет радіофізики, електроніки та комп'ютерних систем"> Факультет радіофізики, електроніки та комп'ютерних систем </option>
        <option value="Факультет соціології"> Факультет соціології </option>
        <option value="Фізичний факультет"> Фізичний факультет </option>
        <option value="Філософський факультет"> Філософський факультет </option>
        <option value="Хімічний факультет"> Хімічний факультет </option>
        <option value="Юридичний факультет"> Юридичний факультет </option>
        <option value="Військовий інститут"> Військовий інститут </option>
        <option value="Інститут високих технологій"> Інститут високих технологій </option>
        <option value="Інститут журналістики"> Інститут журналістики </option>
        <option value="Інститут міжнародних відносин"> Інститут міжнародних відносин </option>
        <option value="Інститут філології"> Інститут філології </option>
 </select>
</p>
<p><input type="text" name="lastname" id="lastname_user" maxlength="40" size="40" placeholder="Введіть прізвище" pattern="[А-ЕЖ-ЩЬЮЯІЇЄ][а-еж-щьюяіїє']*" required/></p>
<p><input type="text" name="name" id="name_user" maxlength="40" size="40" placeholder="Введіть ім'я" pattern="[А-ЕЖ-ЩЬЮЯІЇЄ][а-еж-щьюяіїє']*" required/></p>
<p><input type="text" name="surname" id="surname_user" maxlength="40" size="40" placeholder="Введіть по-батькові" pattern="[А-ЕЖ-ЩЬЮЯІЇЄ][а-еж-щьюяіїє']*" /></p>
<p><input type="text" name="login" id="login" maxlength="40" size="40" placeholder="Придумайте та введіть логін" pattern="[a-zA-Z0-9]*" required/></p>
<p><input type="email" name="email" id="email" maxlength="40" size="40" placeholder="Введіть email" pattern="([_a-z0-9-]+(\.[_a-z0-9-])*@[a-z0-9-]+(\.[a-z0-9-])*(\.[a-z]{2,}))" /></p>
<p>Дані вказані мною вище не є моїми персональними даними і я дозволяю їх зберігання та обробку.<input type="checkbox" onchange="Checkbox()" id="checkbox"/></p>
<p><input type="submit" id="reg" value="Зареєструватись" disabled></p>
  </form>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
                <script src="registrationscript.js"></script>
 </body>
</html>
